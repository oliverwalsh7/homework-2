// primitive_types1.rs
// Fill in the rest of the line that has code missing!
// No hints, there's no tricks, just get used to typing these :)

#[test]
fn main() {
    // Booleans (`bool`)

    let is_morning:bool = true;
    let is_evening:bool = false;

    if is_morning {
        println!("Good morning!");
    }

    if is_evening {
        println!("Good evening!");
    }
}
